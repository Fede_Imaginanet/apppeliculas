<header id="mainHeader" >

  <nav id="mainMenu">
    <ul class="nav nav-fill nav-pills justify-content-center">
      <li class="nav-item active">
        <a class="nav-link active" href="#">Opcion 1</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Opcion 2</a>
       </li>
      <li class="nav-item pt-0">
        <a class="nav-link" href="#">Opcion 3</a>
       </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Opcion 4</a>
       </li>
    </ul>
  </nav>



  <div id="mainSlider" class="carousel slide" data-ride="carousel">

    <!--Indicadores del slider-->
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <!-- End Indicadores del slider-->


    <!--Elementos Slider-->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100 img-slider" src="{{ asset('img/slide1.jpg') }}" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100 img-slider" src="{{ asset('img/slide1.jpg') }}" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100 img-slider" src="{{ asset('img/slide1.jpg') }}" alt="Third slide">
      </div>
    </div>
    <!--Elementos Slider-->


    <!--Controladores de Slider-->
    <a class="carousel-control-prev" href="#mainSlider" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#mainSlider" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    <!--Controladores de Slider-->

  </div> <!--End mainSlide-->

</header><!--   End Header -->
