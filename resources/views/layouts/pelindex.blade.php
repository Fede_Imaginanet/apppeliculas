<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Aplicación de peliculas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @section ('estiloscripts')
    <!-- <link rel="stylesheet" type="text/css" href="/public/css/bootstrap-4-1-3.css" /> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-4-1-3.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/mymain.css') }}" />

    <script type="text/javascript" src="{{ asset('js/jquery-3-3-1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/ajax-popper-1-14-3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-4-1-3.js') }}"></script>

    </script>
    @show
  </head>
  <body>
    @include('cabecera')

    @yield('body')

    @include('pie')
  </body>
</html>
